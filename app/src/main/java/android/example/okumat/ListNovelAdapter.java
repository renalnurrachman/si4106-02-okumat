package com.example.androidnovel;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.androidnovel.Novel.Detail.Novel10DetailActivity;
import com.example.androidnovel.Novel.Detail.Novel11DetailActivity;
import com.example.androidnovel.Novel.Detail.Novel12DetailActivity;
import com.example.androidnovel.Novel.Detail.Novel13DetailActivity;
import com.example.androidnovel.Novel.Detail.Novel14DetailActivity;
import com.example.androidnovel.Novel.Detail.Novel15DetailActivity;
import com.example.androidnovel.Novel.Detail.Novel1DetailActivity;
import com.example.androidnovel.Novel.Detail.Novel2DetailActivity;
import com.example.androidnovel.Novel.Detail.Novel3DetailActivity;
import com.example.androidnovel.Novel.Detail.Novel4DetailActivity;
import com.example.androidnovel.Novel.Detail.Novel5DetailActivity;
import com.example.androidnovel.Novel.Detail.Novel6DetailActivity;
import com.example.androidnovel.Novel.Detail.Novel7DetailActivity;
import com.example.androidnovel.Novel.Detail.Novel8DetailActivity;
import com.example.androidnovel.Novel.Detail.Novel9DetailActivity;

import java.util.ArrayList;

public class ListNovelAdapter extends RecyclerView.Adapter<ListNovelAdapter.ListViewHolder> {
    private ArrayList<NovelModel> listNovelModel;
    private ArrayList<Activity> novelActivty;

    public ListNovelAdapter(ArrayList<NovelModel> list) {
        this.listNovelModel = list;
    }

    class ListViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle;
        TextView tvAuthor;
        ImageView img;
        TextView tvMainGenre;
        TextView tvType;
        TextView tvSynopsis;

        ListViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tv_novel_title);
            tvAuthor = itemView.findViewById(R.id.tv_novel_author);
            img = itemView.findViewById(R.id.img_novel);
            tvMainGenre = itemView.findViewById(R.id.tv_novel_mainGenre);
            tvType = itemView.findViewById(R.id.tv_novel_type);
            tvSynopsis = itemView.findViewById(R.id.tv_novel_synopsis);
        }
    }

    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_row_novel, viewGroup, false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ListViewHolder holder, final int position) {
        final NovelModel novelModel = listNovelModel.get(position);
        RequestOptions options = RequestOptions
                .centerCropTransform()
                .placeholder(R.mipmap.ic_launcher_round)
                .error(R.mipmap.ic_launcher_round);
        Glide.with(holder.itemView.getContext()).load(novelModel.getImage()).apply(options)
                .into(holder.img);


        holder.tvTitle.setText(novelModel.getTitle());
        if (novelModel.getTitle().length() >= 20)
            holder.tvTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, (float) 16.0);
        holder.tvAuthor.setText(novelModel.getAuthor());
        holder.tvMainGenre.setText(novelModel.getMainGenre());
        holder.tvType.setText(novelModel.getType());
        holder.tvSynopsis.setText(novelModel.getSynopsis());

        if (novelModel.getSynopsis() != null && novelModel.getSynopsis().length() >= 120) {
            String snipSynopsis = novelModel.getSynopsis().substring(0, 120) + "...";
            holder.tvSynopsis.setText(snipSynopsis);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = null;
                switch (position) {
                    case 0:
                        intent = new Intent(v.getContext(), Novel1DetailActivity.class);
                        break;
                    case 1:
                        intent = new Intent(v.getContext(), Novel2DetailActivity.class);
                        break;
                    case 2:
                        intent = new Intent(v.getContext(), Novel3DetailActivity.class);
                        break;
                    case 3:
                        intent = new Intent(v.getContext(), Novel4DetailActivity.class);
                        break;
                    case 4:
                        intent = new Intent(v.getContext(), Novel5DetailActivity.class);
                        break;
                    case 5:
                        intent = new Intent(v.getContext(), Novel6DetailActivity.class);
                        break;
                    case 6:
                        intent = new Intent(v.getContext(), Novel7DetailActivity.class);
                        break;
                    case 7:
                        intent = new Intent(v.getContext(), Novel8DetailActivity.class);
                        break;
                    case 8:
                        intent = new Intent(v.getContext(), Novel9DetailActivity.class);
                        break;
                    case 9:
                        intent = new Intent(v.getContext(), Novel10DetailActivity.class);
                        break;
                    case 10:
                        intent = new Intent(v.getContext(), Novel11DetailActivity.class);
                        break;
                    case 11:
                        intent = new Intent(v.getContext(), Novel12DetailActivity.class);
                        break;
                    case 12:
                        intent = new Intent(v.getContext(), Novel13DetailActivity.class);
                        break;
                    case 13:
                        intent = new Intent(v.getContext(), Novel14DetailActivity.class);
                        break;
                    case 14:
                        intent = new Intent(v.getContext(), Novel15DetailActivity.class);
                        break;
                    default:
                        break;
                }
                if (intent != null) {
                    intent.putExtra("novel_title", novelModel.getTitle());
                    intent.putExtra("novel_author", novelModel.getAuthor());
                    intent.putExtra("novel_type", novelModel.getType());
                    intent.putExtra("novel_image", novelModel.getImage());
                    intent.putExtra("novel_release", novelModel.getRelease());
                    intent.putExtra("novel_genre", novelModel.getGenre());
                    intent.putExtra("novel_synopsis", novelModel.getSynopsis());
                    intent.putExtra("novel_preview", novelModel.getPreview());
                    intent.putExtra("novel_status", novelModel.getStatus());
                    intent.putExtra("novel_position", novelModel.getPosition());
                    v.getContext().startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return listNovelModel.size();
    }

}