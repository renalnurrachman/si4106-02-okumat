package com.example.androidnovel;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.androidnovel.Novel.Detail.Novel1DetailActivity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class MainActivity extends AppCompatActivity {
    private RecyclerView rvNovel;
    private ArrayList<NovelModel> list;
    public DatabaseReference reference;
    public ProgressBar progressBar;
    public ImageView ivNoConn;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_main);
        list = new ArrayList<>();
        ivNoConn = findViewById(R.id.iv_no_connect);
        progressBar = findViewById(R.id.pb_loading);
        progressBar.setIndeterminate(true);

        if (this.verifyAvailableNetwork(this)) {

            reference = FirebaseDatabase.getInstance().getReference().child("Novel");
            reference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot p0) {

                    if (!list.isEmpty()) list.clear();
                    progressBar.setIndeterminate(false);
                    progressBar.setVisibility(View.GONE);

                    int position = 0;
                    for (DataSnapshot dataSnapshot : p0.getChildren()) {
                        NovelModel novelModel = dataSnapshot.getValue(NovelModel.class);
                        if (novelModel != null) {
                            list.add(novelModel);
                            list.get(position).setPosition(position);
                        }
                        position++;
                    }
                    rvNovel = findViewById(R.id.rv_novel);
                    rvNovel.setHasFixedSize(true);
                    showRecyclerList();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                }

            });
        } else {
            progressBar.setIndeterminate(false);
            progressBar.setVisibility(View.GONE);
            ivNoConn.setVisibility(View.VISIBLE);
        }
    }

    private void warn(String warning) {
        Toast.makeText(this, warning, Toast.LENGTH_SHORT).show();
    }

    private void showRecyclerList() {
        rvNovel.setLayoutManager(new LinearLayoutManager(this));
        ListNovelAdapter listNovelAdapter = new ListNovelAdapter(list);
        rvNovel.setAdapter(listNovelAdapter);
    }

    public final boolean verifyAvailableNetwork(@NotNull AppCompatActivity activity) {
        ConnectivityManager connectivityManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
