package com.example.androidnovel;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;

public class LibraryActivity extends AppCompatActivity {
    DatabaseReference libraryRef;
    LibraryModel libraryModel;
    ArrayList<LibraryModel> listLibrary;
    DocumentReference documentReference;
    RecyclerView rvLibrary;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_library);
        setTitle("OKUMAT");

        listLibrary = new ArrayList<>();
        libraryRef = FirebaseDatabase.getInstance().getReference().child("Library");
        libraryRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot p0) {
                if (!listLibrary.isEmpty()) listLibrary.clear();
                for (DataSnapshot dataSnapshot : p0.getChildren()) {
                    LibraryModel libraryModel = dataSnapshot.getValue(LibraryModel.class);
                    if (libraryModel != null) {
                        listLibrary.add(libraryModel);
                    }
                }
                rvLibrary = findViewById(R.id.rv_library);
                rvLibrary.setHasFixedSize(true);
                showRecyclerList();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void showRecyclerList() {
        rvLibrary.setLayoutManager(new LinearLayoutManager(this));
        ListLibraryAdapter listLibraryAdapter = new ListLibraryAdapter(listLibrary);
        rvLibrary.setAdapter(listLibraryAdapter);
    }
}