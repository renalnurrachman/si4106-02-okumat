package com.example.androidnovel;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;

public class FeedbackActivity extends AppCompatActivity {
    EditText etComment;
    Button btnSend;
    DatabaseReference commentRef;
    FirebaseDatabase database;
    Boolean commentChecker;
    FirebaseFirestore fStore;
    String currentUserId;
    ArrayList<CommentModel> listComment;
    DocumentReference documentReference;
    RecyclerView rvComment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        etComment = findViewById(R.id.et_comment);
        btnSend = findViewById(R.id.btn_send);
        commentChecker = false;
        fStore = FirebaseFirestore.getInstance();
        listComment = new ArrayList<>();
        setTitle("OKUMAT");

        currentUserId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        documentReference = fStore.document("users/" + currentUserId);
        database = FirebaseDatabase.getInstance();
        String novelTitle = getIntent().getStringExtra("novel_title");
        commentRef = FirebaseDatabase.getInstance().getReference().child("Feedback");
        commentRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot p0) {
                if (!listComment.isEmpty()) listComment.clear();
                for (DataSnapshot dataSnapshot : p0.getChildren()) {
                    CommentModel commentModel = dataSnapshot.getValue(CommentModel.class);
                    if (commentModel != null) {
                        listComment.add(commentModel);
                    }
                }
                rvComment = findViewById(R.id.rv_comment);
                rvComment.setHasFixedSize(true);
                showRecyclerList();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        btnSend.setOnClickListener(new View.OnClickListener() {
            String key = database.getReference().push().getKey();

            @Override
            public void onClick(View v) {
                commentChecker = true;
                commentRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot p0) {
                        if (commentChecker) {
                            documentReference.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                @Override
                                public void onSuccess(DocumentSnapshot documentSnapshot) {
                                    if (documentSnapshot.exists()) {
                                        String username = documentSnapshot.getString("uname");
                                        String comment = etComment.getText().toString();
                                        commentRef.child(key).child("username").setValue(username);
                                        commentRef.child(key).child("comment").setValue(comment);
                                        commentChecker = false;
                                        btnSend.setText("Edit");
                                    } else {
                                        Toast.makeText(getApplicationContext(), "Document not exist", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
                                }
                            });
                            if (!listComment.isEmpty()) listComment.clear();
                            for (DataSnapshot dataSnapshot : p0.getChildren()) {
                                CommentModel commentModel = dataSnapshot.getValue(CommentModel.class);
                                if (commentModel != null) {
                                    listComment.add(commentModel);
                                }
                            }
                            rvComment = findViewById(R.id.rv_comment);
                            rvComment.setHasFixedSize(true);
                            showRecyclerList();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });
    }

    private void showRecyclerList() {
        rvComment.setLayoutManager(new LinearLayoutManager(this));
        ListCommentAdapter listCommentAdapter = new ListCommentAdapter(listComment);
        rvComment.setAdapter(listCommentAdapter);
    }
}

