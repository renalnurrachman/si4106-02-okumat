package com.example.androidnovel;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class ListLibraryAdapter extends RecyclerView.Adapter<ListLibraryAdapter.ListViewHolder> {
    private ArrayList<LibraryModel> listLibraryModel;

    public ListLibraryAdapter(ArrayList<LibraryModel> list) {
        this.listLibraryModel = list;
    }

    class ListViewHolder extends RecyclerView.ViewHolder {
        TextView tvName;
        TextView tvAlamat;
        TextView tvBuka;
        TextView tvFasilitas;
        ListViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_novel_title);
            tvAlamat = itemView.findViewById(R.id.tv_novel_genre);
            tvBuka = itemView.findViewById(R.id.tv_buka);
            tvFasilitas = itemView.findViewById(R.id.tv_fasilitas);
        }
    }

    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_row_library, viewGroup, false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ListViewHolder holder, final int position) {
        LibraryModel libraryModel = listLibraryModel.get(position);
        holder.tvName.setText(libraryModel.nama);
        holder.tvAlamat.setText(libraryModel.alamat);
        holder.tvBuka.setText(libraryModel.buka);
        holder.tvFasilitas.setText(libraryModel.fasilitas);
    }

    @Override
    public int getItemCount() {
        return listLibraryModel.size();
    }

}