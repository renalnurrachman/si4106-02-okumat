package com.example.androidnovel;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

public class NovelDetailActivity extends AppCompatActivity {

    public TextView tvNovelTitle;
    public TextView tvNovelAuthor;
    public TextView tvNovelType;
    public ImageView ivNovelImage;
    public TextView tvNovelRelease;
    public TextView tvNovelGenre;
    public TextView tvNovelSynopsis;
    public Button btnReadPreview;
    public TextView tvNovelStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_novel);
        setTitle("OKUMAT");

        tvNovelTitle = findViewById(R.id.tv_novel_title);
        tvNovelAuthor = findViewById(R.id.tv_novel_author);
        tvNovelType = findViewById(R.id.tv_novel_type);
        ivNovelImage = findViewById(R.id.iv_novel_image);
        tvNovelRelease = findViewById(R.id.tv_novel_release);
        tvNovelGenre = findViewById(R.id.tv_novel_genre);
        tvNovelSynopsis = findViewById(R.id.tv_novel_preview);
        btnReadPreview = findViewById(R.id.btn_read_preview);
        tvNovelStatus = findViewById(R.id.tv_novel_status);


        final String title = getIntent().getStringExtra("novel_title");
        final String author = getIntent().getStringExtra("novel_author");
        final String preview = getIntent().getStringExtra("novel_preview");

        tvNovelTitle.setText(title);
        tvNovelAuthor.setText(author);
        tvNovelType.setText(getIntent().getStringExtra("novel_type"));

        RequestOptions options = RequestOptions
                .centerCropTransform()
                .placeholder(R.mipmap.ic_launcher_round)
                .error(R.mipmap.ic_launcher_round);

        Glide.with(this).load(getIntent().getStringExtra("novel_image")).apply(options)
                .into(ivNovelImage);
        tvNovelRelease.setText(getIntent().getStringExtra("novel_release"));
        tvNovelGenre.setText(getIntent().getStringExtra("novel_genre"));
        tvNovelStatus.setText(getIntent().getStringExtra("novel_status"));
        tvNovelSynopsis.setText(getIntent().getStringExtra("novel_synopsis"));
        btnReadPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), NovelPreviewActivity.class);
                intent.putExtra("novel_title", title);
                intent.putExtra("novel_author", author);
                intent.putExtra("novel_preview", preview);
                startActivity(intent);
            }
        });
    }
}
