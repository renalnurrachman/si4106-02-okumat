package com.example.androidnovel;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class WriteBookActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    EditText etTitle;
    Spinner spinGenre;
    EditText etIsi;
    Button btnPublish;
    DatabaseReference bookRef;
    String currentUserId;
    String genre = "";
    FirebaseFirestore fStore;
    DocumentReference documentReference;
    String author;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_write_book);
        etTitle = findViewById(R.id.et_title);
        spinGenre = findViewById(R.id.spin_genre);
        etIsi = findViewById(R.id.et_isi);
        btnPublish = findViewById(R.id.btn_publish);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.genre, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinGenre.setAdapter(adapter);
        spinGenre.setOnItemSelectedListener(this);
        currentUserId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        fStore = FirebaseFirestore.getInstance();
        documentReference = fStore.document("users/" + currentUserId);
        setTitle("OKUMAT");

        btnPublish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String title = etTitle.getText().toString();
                final String isi = etIsi.getText().toString();
                if (title.isEmpty()) {
                    etTitle.setError("Isi Judul Buku");
                    return;
                }
                if (isi.isEmpty()) {
                    etTitle.setError("Isi Isi Buku");
                    return;
                }
                if (genre.equals("")) {
                    Toast.makeText(getApplicationContext(), "Pilih Genre", Toast.LENGTH_SHORT).show();
                    return;
                }

                bookRef = FirebaseDatabase.getInstance().getReference().child("Books").child(title);
                bookRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        documentReference.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                            @Override
                            public void onSuccess(DocumentSnapshot documentSnapshot) {
                                if (documentSnapshot.exists()) {
                                    author = documentSnapshot.getString("uname");
                                    bookRef.child("title").setValue(title);
                                    bookRef.child("author").setValue(author);
                                    bookRef.child("genre").setValue(genre);
                                    bookRef.child("isi").setValue(isi);
                                } else {
                                    Toast.makeText(WriteBookActivity.this, "User not exist", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                        startActivity(new Intent(WriteBookActivity.this,BooksActivity.class));
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        genre = parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}