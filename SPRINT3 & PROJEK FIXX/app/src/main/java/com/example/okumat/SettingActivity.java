package com.example.androidnovel;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class SettingActivity extends AppCompatActivity {
    Button btnHelp;
    Button btnAbouApp;
    Button btnInfoDeveloper;
    Button btnFeedback;
    Button btnContactUs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        setTitle("OKUMAT");

        btnHelp = findViewById(R.id.btn_help);
        btnAbouApp = findViewById(R.id.btn_about_app);
        btnInfoDeveloper = findViewById(R.id.btn_info_developer);
        btnFeedback = findViewById(R.id.btn_feedback);
        btnContactUs = findViewById(R.id.btn_contact_us);

        btnHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingActivity.this,HelpActivity.class));
            }
        });

        btnAbouApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingActivity.this,AboutAppActivity.class));
            }
        });

        btnInfoDeveloper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingActivity.this,InfoDeveloperActivity.class));
            }
        });

        btnFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingActivity.this,FeedbackActivity.class));
            }
        });

        btnContactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingActivity.this,ContactUsActivity.class));
            }
        });
    }
}
