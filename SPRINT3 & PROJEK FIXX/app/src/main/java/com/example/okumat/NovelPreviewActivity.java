package com.example.androidnovel;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class NovelPreviewActivity extends AppCompatActivity {
    TextView tvNovelTitle;
    TextView tvNovelAuthor;
    TextView tvNovelPreview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_novel_preview);

        tvNovelTitle = findViewById(R.id.tv_novel_title);
        tvNovelAuthor = findViewById(R.id.tv_novel_author);
        tvNovelPreview = findViewById(R.id.tv_novel_preview);

        String title = getIntent().getStringExtra("novel_title");
        String author = getIntent().getStringExtra("novel_author");
        String preview = getIntent().getStringExtra("novel_preview");

        tvNovelTitle.setText(title);
        tvNovelAuthor.setText(author);
        tvNovelPreview.setText(preview);
    }

}
