package com.example.androidnovel;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {
    private EditText etUsername;
    private EditText etEmail;
    private EditText etPassword;
    private EditText etCPassword;
    private Button btnRegister;
    private FirebaseAuth fAuth;
    private FirebaseFirestore fStore;
    private int RESULT_LOAD_IMG = 1;
    private String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        init();
        listener();
    }

    private void init() {
        etUsername = findViewById(R.id.et_username);
        etEmail = findViewById(R.id.et_email);
        etPassword = findViewById(R.id.et_password);
        etCPassword = findViewById(R.id.et_cpassword);
        btnRegister = findViewById(R.id.btn_register);
        fAuth = FirebaseAuth.getInstance();
        fStore = FirebaseFirestore.getInstance();
        setTitle("OKUMAT");
    }

    private void listener() {

        //Fungsi ketika tombol register di klik
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String email = etEmail.getText().toString();
                String password = etPassword.getText().toString();
                final String username = etUsername.getText().toString();

                if (etUsername.getText().toString().isEmpty()){
                    etUsername.setError("Username is reqruied!");
                    return;
                }
                if (etEmail.getText().toString().isEmpty()) {
                   etEmail.setError("Email is reqruied!");
                   return;
                }
                if (etPassword.getText().toString().isEmpty()) {
                    etPassword.setError("Password is reqruied!");
                    return;
                }
                if (etCPassword.getText().toString().isEmpty()) {
                    etCPassword.setError("Confirm Password is reqruied!");
                    return;
                }
                if(!etCPassword.getText().toString().equals(etCPassword.getText().toString())){
                    etPassword.setError("Password and Confirm Password are not the same!");
                    return;
                }

                //Membuat user baru dengan email dan password di Firebase
                fAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()) {
                            userId = fAuth.getCurrentUser().getUid();
                            DocumentReference documentReference = fStore.collection("users").document(userId);
                            Map<String, Object> user = new HashMap<>();
                            user.put("uname", username);
                            user.put("email", email);
                            documentReference.set(user).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Toast.makeText(getApplicationContext(), "Register sukses", Toast.LENGTH_SHORT).show();
                                }
                            });
                            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                        }else{
                            Toast.makeText(getApplicationContext(), task.getException().toString(), Toast.LENGTH_SHORT).show();

                        }
                    }
                });
            }
        });
    }
}

