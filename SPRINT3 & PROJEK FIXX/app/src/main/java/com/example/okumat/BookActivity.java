package com.example.androidnovel;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class BookActivity extends AppCompatActivity {
    TextView tvTitle;
    TextView tvAuthor;
    TextView tvGenre;
    TextView tvIsi;
    DatabaseReference bookRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);
        String title = getIntent().getStringExtra("book_title");
        tvTitle = findViewById(R.id.tv_novel_title);
        tvAuthor = findViewById(R.id.tv_novel_author);
        tvGenre = findViewById(R.id.tv_novel_genre);
        tvIsi = findViewById(R.id.tv_novel_preview);
        bookRef = FirebaseDatabase.getInstance().getReference().child("Books").child(title);
        setTitle("OKUMAT");

        bookRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot p0) {
                BookModel bookModel = p0.getValue(BookModel.class);
                tvTitle.setText(bookModel.title);
                tvAuthor.setText(bookModel.author);
                tvGenre.setText(bookModel.genre);
                tvIsi.setText(bookModel.isi);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }
}
