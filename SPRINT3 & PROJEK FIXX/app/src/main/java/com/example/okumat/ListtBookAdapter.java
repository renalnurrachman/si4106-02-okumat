package com.example.androidnovel;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class ListtBookAdapter extends RecyclerView.Adapter<ListtBookAdapter.ListViewHolder> {
    private ArrayList<BookModel> listBookModel;

    public ListtBookAdapter(ArrayList<BookModel> list) {
        this.listBookModel = list;
    }

    class ListViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle;
        TextView tvAuthor;
        TextView tvGenre;
        ListViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tv_novel_title);
            tvAuthor = itemView.findViewById(R.id.tv_author);
            tvGenre = itemView.findViewById(R.id.tv_novel_genre);
        }
    }

    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_row_books, viewGroup, false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ListViewHolder holder, final int position) {
        final BookModel bookModel = listBookModel.get(position);
        holder.tvAuthor.setText(bookModel.author);
        holder.tvTitle.setText(bookModel.title);
        holder.tvGenre.setText(bookModel.genre);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(),BookActivity.class);
                intent.putExtra("book_title",bookModel.title);
                v.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listBookModel.size();
    }

}