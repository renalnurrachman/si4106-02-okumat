package com.example.androidnovel;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class BooksActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    DatabaseReference bookRef;
    FirebaseDatabase database;
    String currentUserId;
    ArrayList<BookModel> listBooks;
    RecyclerView rvBook;
    FloatingActionButton fbtnWrite;
    EditText etSearch;
    Spinner spinGenre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_books);
        listBooks = new ArrayList<>();
        etSearch = findViewById(R.id.et_search);
        fbtnWrite = findViewById(R.id.fbtn_write_book);
        spinGenre = findViewById(R.id.spin_genre);
        rvBook = findViewById(R.id.rv_book);
        setTitle("OKUMAT");
        currentUserId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        bookRef = FirebaseDatabase.getInstance().getReference().child("Books");
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.genre, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinGenre.setAdapter(adapter);
        spinGenre.setOnItemSelectedListener(this);
        bookRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot p0) {
                if (!listBooks.isEmpty()) listBooks.clear();
                for (DataSnapshot dataSnapshot : p0.getChildren()) {
                    BookModel bookModel = dataSnapshot.getValue(BookModel.class);
                    if (bookModel != null) {
                        listBooks.add(bookModel);
                    }
                }
                rvBook.setHasFixedSize(true);
                showRecyclerList(listBooks);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        fbtnWrite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(BooksActivity.this, WriteBookActivity.class));
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().isEmpty()) {
                    search(s.toString());
                } else {
                    search("");
                }
            }
        });
    }

    private void showRecyclerList(ArrayList<BookModel> list) {
        rvBook.setLayoutManager(new LinearLayoutManager(this));
        ListtBookAdapter listtBookAdapter = new ListtBookAdapter(list);
        rvBook.setAdapter(listtBookAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent a = new Intent(BooksActivity.this, MainActivity.class);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

    public void search(String text) {
        ArrayList<BookModel> search = new ArrayList<>();
        for (BookModel book : listBooks) {
            if (book.title.toLowerCase().contains(text.toLowerCase())) {
                search.add(book);
            }
        }
        showRecyclerList(search);
    }

    public void searchGenre(String genre) {
        ArrayList<BookModel> search = new ArrayList<>();
        for (BookModel book : listBooks) {
            if (book.genre.toLowerCase().contains(genre.toLowerCase())) {
                search.add(book);
            }
        }
        showRecyclerList(search);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String genre = parent.getItemAtPosition(position).toString();
        searchGenre(genre);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}